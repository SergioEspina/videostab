import numpy as np
import cv2 as cv
import math
from matplotlib import pyplot as plt
import pandas as pd
import statistics as st
from sklearn import metrics


videoU = "./output/test59-50.avi"
videoS = "./dataset/stable/59.avi"
capU = cv.VideoCapture(videoU)
capS = cv.VideoCapture(videoS)

featureParams = {
    "maxCorners" : 100,
    "qualityLevel" : 0.3,
    "minDistance" : 7,
    "blockSize" : 7
}

# Step 1: Calculate transforms unstable and extract translation and rotation

ret, old_frame = capU.read()
old_gray = cv.cvtColor(old_frame, cv.COLOR_BGR2GRAY)
pointsOld = cv.goodFeaturesToTrack(old_gray,**featureParams)

transformsU = []

while(True):
    ret, cur_frame = capU.read()
    if (not ret):
        break
    cur_gray = cv.cvtColor(cur_frame, cv.COLOR_BGR2GRAY)

    # Optical flow: https://docs.opencv.org/3.4/d4/dee/tutorial_optical_flow.html
    pointsCur, status, error = cv.calcOpticalFlowPyrLK(old_gray, cur_gray, pointsOld, None)

    # Select only good matches
    if pointsCur is not None:
        pointsCurGood = pointsCur[status==1]
        pointsOldGood = pointsOld[status==1]
    
    # Calculate transformations 
    transform = cv.estimateAffinePartial2D(pointsOldGood,pointsCurGood)
    if transform is None:
        transform = transformsU[-1]
    
    transformsU.append(transform)



    # Current to old
    old_gray = cur_gray.copy()
    p0 = pointsCurGood.reshape(-1,1,2)

trajectoryXU = []
trajectoryYU = []
trajectoryAngleU = []

for i in range(len(transformsU)):
    tr = transformsU[i]
    # Explanation of the matrix: https://math.stackexchange.com/questions/13150/extracting-rotation-scale-values-from-2d-transformation-matrix
    dx=tr[0][0][2]
    dy=tr[0][1][2]
    dAngle=math.atan2(tr[0][1][0],tr[0][1][1])

    trajectoryXU.append(dx)
    trajectoryYU.append(dy)
    trajectoryAngleU.append(dAngle)

# Step 2: Calculate transforms stable and extract translation and rotation

ret, old_frame = capS.read()
old_gray = cv.cvtColor(old_frame, cv.COLOR_BGR2GRAY)
pointsOld = cv.goodFeaturesToTrack(old_gray,**featureParams)

transformsS = []

while(True):
    ret, cur_frame = capS.read()
    if (not ret):
        break
    cur_gray = cv.cvtColor(cur_frame, cv.COLOR_BGR2GRAY)

    # Optical flow: https://docs.opencv.org/3.4/d4/dee/tutorial_optical_flow.html
    pointsCur, status, error = cv.calcOpticalFlowPyrLK(old_gray, cur_gray, pointsOld, None)

    # Select only good matches
    if pointsCur is not None:
        pointsCurGood = pointsCur[status==1]
        pointsOldGood = pointsOld[status==1]
    
    
    # Calculate transformations 
    transform = cv.estimateAffinePartial2D(pointsOldGood,pointsCurGood)
    transformsS.append(transform)

    # Current to old
    old_gray = cur_gray.copy()
    p0 = pointsCurGood.reshape(-1,1,2)

trajectoryXS = []
trajectoryYS = []
trajectoryAngleS = []

for i in range(len(transformsS)):
    tr = transformsS[i]
    # Explanation of the matrix: https://math.stackexchange.com/questions/13150/extracting-rotation-scale-values-from-2d-transformation-matrix
    dx=tr[0][0][2]
    dy=tr[0][1][2]
    dAngle=math.atan2(tr[0][1][0],tr[0][1][1])

    trajectoryXS.append(dx)
    trajectoryYS.append(dy)
    trajectoryAngleS.append(dAngle)

#Print cosine_similarity
print(metrics.pairwise.cosine_similarity([trajectoryXS],[trajectoryXU]))
print(metrics.pairwise.cosine_similarity([trajectoryYS],[trajectoryYU]))
print(metrics.pairwise.cosine_similarity([trajectoryAngleS],[trajectoryAngleU]))