import numpy as np
import cv2 as cv
import math
from matplotlib import pyplot as plt

# Setup

video = "./dataset/unstable/19.avi"
output = "./output/test19-10.avi"
cap = cv.VideoCapture(video)

featureParams = {
    "maxCorners" : 100,
    "qualityLevel" : 0.3,
    "minDistance" : 7,
    "blockSize" : 7
}

# Step 1: Calculate transforms using optical flow

ret, old_frame = cap.read()
old_gray = cv.cvtColor(old_frame, cv.COLOR_BGR2GRAY)
pointsOld = cv.goodFeaturesToTrack(old_gray,**featureParams)

transforms = []

while(True):
    ret, cur_frame = cap.read()
    if (not ret):
        break
    cur_gray = cv.cvtColor(cur_frame, cv.COLOR_BGR2GRAY)

    # Optical flow: https://docs.opencv.org/3.4/d4/dee/tutorial_optical_flow.html
    pointsCur, status, error = cv.calcOpticalFlowPyrLK(old_gray, cur_gray, pointsOld, None)

    # Select only good matches
    if pointsCur is not None:
        pointsCurGood = pointsCur[status==1]
        pointsOldGood = pointsOld[status==1]
    
    
    # Calculate transformations 
    transform = cv.estimateAffinePartial2D(pointsOldGood,pointsCurGood)
    transforms.append(transform)

    # Current to old
    old_gray = cur_gray.copy()
    p0 = pointsCurGood.reshape(-1,1,2)

# Step 2 calculate trajectory

trajectoryX = []
trajectoryY = []
trajectoryAngle = []

for i in range(len(transforms)):
    tr = transforms[i]
    # Explanation of the matrix: https://math.stackexchange.com/questions/13150/extracting-rotation-scale-values-from-2d-transformation-matrix
    dx=tr[0][0][2]
    dy=tr[0][1][2]
    dAngle=math.atan2(tr[0][1][0],tr[0][1][1])

    if(i == 0):
        trajectoryX.append(dx)
        trajectoryY.append(dy)
        trajectoryAngle.append(dAngle)
    else:
        trajectoryX.append(trajectoryX[i-1] + dx)
        trajectoryY.append(trajectoryY[i-1] + dy)
        trajectoryAngle.append(trajectoryAngle[i-1] + dAngle)

# Step 3 smooth trajectory
window = 30
max = len(transforms)

smoothX = []
smoothY = []
smoothAngle = []

for i in range(max):
    limitL = 0
    limitR = max
    if(i - window > 0):
        limitL = i - window
    if(i + window < max):
        limitR = i + window    

    indexes = range(limitL,limitR)
    listX = [trajectoryX[index] for index in indexes]
    listY = [trajectoryY[index] for index in indexes]
    listAngle = [trajectoryAngle[index] for index in indexes]

    avgX = sum(listX)/float(len(listX))
    smoothX.append(avgX)

    avgY = sum(listY)/float(len(listY))
    smoothY.append(avgY)

    avgAngle = sum(listAngle)/float(len(listAngle))
    smoothAngle.append(avgAngle)
fig, ax = plt.subplots(2,2)

plt.suptitle("Trajectories")

ax[0,0].plot(trajectoryX,label="Traj X")
ax[0,0].plot(smoothX,label="Smooth X")
ax[0,0].set_title("X")
ax[0,1].plot(trajectoryY,label="Traj Y")
ax[0,1].plot(smoothY,label="Smooth Y")
ax[0,1].set_title("Y")
ax[1,0].plot(trajectoryAngle,label="Traj Angle")
ax[1,0].plot(smoothAngle,label="Smooth Angle")
ax[1,0].set_title("θ")

plt.show()

# Step 4 calculate new transforms

newTransforms = transforms.copy()

for i in range(len(newTransforms)):
    tr = newTransforms[i]

    ndx = tr[0][0][2] + (smoothX[i] - trajectoryX[i])
    ndy = tr[0][1][2] + (smoothY[i] - trajectoryY[i])
    ndAngle= math.atan2(tr[0][1][0],tr[0][1][1]) + (smoothAngle[i] - trajectoryAngle[i])

    tr[0][0][2] = ndx
    tr[0][1][2] = ndy

    tr[0][0][0] = math.cos(ndAngle)
    tr[0][0][1] = -1 * math.sin(ndAngle)
    tr[0][1][0] = math.sin(ndAngle)
    tr[0][1][1] = math.cos(ndAngle)

    newTransforms[i] = tr

# Step 5 apply new transformations

cap.set(cv.CAP_PROP_POS_FRAMES,0)
frames = cap.get(cv.CAP_PROP_FRAME_COUNT)

frameRate = cap.get(cv.CAP_PROP_FPS)
size = (cap.get(cv.CAP_PROP_FRAME_WIDTH), cap.get(cv.CAP_PROP_FRAME_HEIGHT))

vw = cv.VideoWriter(output,cv.VideoWriter_fourcc(*'DIVX'),frameRate,(1280,720))
cv.VideoWriter()

for i in range(int(frames)-1):
  ret, frame = cap.read()
  transformedF = cv.warpAffine(frame,newTransforms[i][0],(1280,720))
  vw.write(transformedF)
ret, frame = cap.read()
vw.write(frame)
vw.release()